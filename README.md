# Github - notes

If you are here to learn more about git or need a refresher, make sure to open a new project where you can experiment with the commands and read about them here.

Otherwise if you are a beginner, please check [this tutorial](https://www.youtube.com/watch?v=SWYqp7iY_Tc) first.

- [What's a local and remote repository?](https://gitlab.com/KavenOZ/github-notes#what-is-the-difference-between-a-local-and-a-remote-repo)
- [What is the staging area?](https://gitlab.com/KavenOZ/github-notes#what-is-the-staging-area)
- [What are branches for?](https://gitlab.com/KavenOZ/github-notes#what-are-branches-for)
- [Difference between `git fetch` and `git pull`]()

## What is github?

Github is a website that provides free hosting for software developement version control using Git. It was made by Linus Torvalds, the creator of the Linux Kernel. It has been aquired by Microsoft in 2018.

## What is git?

Git is a decentralized Version Control System ( VCS ), what means that developers can develop an app while not being on the same network.

Git:
- Keeps track of code history of your project.
- Takes "snapshots" of your files.
- You decide when to make a snapshot by making a commit.
- You can visit any snapshot in any time.
- You can stage files before committing.

## Common git commands

Some of the more common commands that the git CLI offers us:

```sh

    $ git init                  # Initializes a local repository

    $ git add <File(s)>         # Adds file(s) changes to the Staging Area ( or sometimes called Index )

    $ git status                # Show info about files that are tracked, modified, and untracked

    $ git commit                # Save changes to local repository

    $ git push <remote repo> <local branch> # Push changes to a remote repository

    $ git pull                  # Pull changes from remote repository to merge with your local repository

    $ git fetch                 # Downloads information about the changes from the remote repo

    $ git clone <Github URL>    # Downloades a repository

    $ git merge                 # Merges a local branch to a different branch OR master

    $ git branch                # Shows all local branches

    $ git checkout              # Change to a different branch or revert changes of file(s) to last commit

    $ git diff                  # Show changes after latest commit

    $ git log                   # Show changes done on local repository

    $ git revert                # Creates a new commit that reverts the changes

    $ git rebase                # Changes base of branch to the latest commit ( Don't actually use this on public commits, only if you know what you're doing )
    
    $ git reset                 # Resets your working directory to the given or last commit
```


## What is the difference between a local and a remote repo?

The main difference between a local and a remote repository is that the local repo is **on your computer** ( git init OR once a project has been cloned ) whereas the remote repo **is online** ( the project that you see on Github, Gitlab or Bitbucket ). You mainly work on a local repository, in the **working directory** to be exact, and when you are ready to throw the change on github, you make a snapshot of the change ( **git commit** ) and push it to the remote repository ( **git push** ).

## What is the staging area?

The staging area is where you add files to be saved, every modification or creation of a file must be added to the staging area if you want to have a snapshot of these files.

<img src="https://miro.medium.com/max/481/0*h0aOKyXxUmlS-dIK.png"/>

## What are branches for?

Think of local and remote repositories as **trees**, and the branches of those trees are the branches of those repositories. Those branches are mainly used to create features or different versions of an application for people to work on together, like 
adding a streaming feature on an application. Helpful for not creating conflict ( like modification of a file ) between local repositories when pushing stuff to a remote one.

To create a branch on a local repository, use the `git checkout -b` command.

<img src="https://tleyden-misc.s3.amazonaws.com/blog_images/release-branching-strategy.png"/>

## What are pull requests?

A pull request is when you want to merge a branch with MASTER or a different remote branch. Before merging, the pull request must be reviewed by the maintainer of the project, so he can decide if the change should be merged, or declined ( in the remote case ). You can also commit changes to the pull request ( so on the remote branch the pull request was made on ) before the pull request is ready for review.

## How do I revert changes? ( Local )

### Removing a file and committing

To remove a file and commit the change, you just need:

```sh
    git rm <file>
```

### Reverting code back to latest commit

Let's say we made a change to a file, and for some reason we can't undo the changes manually:

```sh
    $ git checkout <file>
```
will refresh the file to the latest commit on the local repository.

### Changing the commit message

So, we made a snapshot of the changes made on the local repository, but there is a typo that we don't want or the description is misleading:

```sh
    $ git commit --amend -m "<Our modified message>"
```
This command will update the commit message.

### We left off a file we wanted to commit

In this case, it is pretty similar to the change in the commit message, first add the file to the staging area, and then:

```sh
    $ git commit --amend
```

### We want to move a commit to a different branch

Let's say that you commited a change, but on a wrong branch and you want to fix that. The first thing we should do is to move the commit to our branch, we can do it using: ( Make sure you are in the branch where you want the commit to be )

```sh
    git cherry-pick <commit-hash>
```

You can find the commit hashes in the `git log` output.

The next thing you want to do is to remove the commit in the old branch, we do that by using:

```sh
    git reset <commit-hash>
```
These are the flags for `git reset` you should know ( I recommend playing with these options to undestand how they work better ):
- --soft: Files are not modified in any way, just the current commit changes.
- --mixed (default option): Files that are not in the new commit are set to untracked and the modified files are unstaged.
- --hard: Modifies and removes all files that have been change, so basically git forces the working directory to look like the one after that given commit.

<img src="https://i.stack.imgur.com/qRAte.jpg">

## Removing a branch after a merge

When a merge occurs, we are left with a local branch that we don't want anymore, to delete it, we use:
```sh
    git branch -D <useless branch>
```
But, if we wanted to remove a remote branch:
```sh
    git push origin --delete <useless remote branch>
```

## The difference between pull and fetch

The main difference between pull and fetch is that fetch retrieves only the information about changes of the project, whereas pull also copies those changes from the remote repo.

### Useful links
- https://www.youtube.com/watch?v=FdZecVxzJbk
- https://www.youtube.com/watch?v=SWYqp7iY_Tc
- https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging
- https://www.freecodecamp.org/news/git-fetch-vs-pull/